package Robot;
import java.util.*;
public class RobotArena {
	int Horizontal, vertical, ID, robotrandX,robotrandY;
	Random randomGenerator = new Random();
	static ArrayList<TheRobots> test = new ArrayList<TheRobots>();
	
	public RobotArena(int X, int Y) {
		//declare random object in constor, soa  random num gets initiated everytije we create object
		//Random randomGenerator = new Random();
		//Horizontal = X;
		//vertical = Y;
		Horizontal =  X;
		vertical = Y;
		robotrandX = randomGenerator.nextInt(Horizontal);
		robotrandY = randomGenerator.nextInt(vertical);
	}
	
	
	public void addRobot() {
		boolean isEmpty = false;
		//Add first robot
		do {
			if(test.size() == 0) {
				robotrandX = randomGenerator.nextInt(Horizontal);
				robotrandY = randomGenerator.nextInt(vertical);
				TheRobots temp = new TheRobots(this.robotrandX,this.robotrandY, Direction.getRandomDirection());
				ID = temp.robotid();
				test.add(temp);
				System.out.println("First");
				isEmpty = true;
			}
			if(this.getRobotAt(robotrandX, robotrandY) != null) {
				robotrandX = randomGenerator.nextInt(Horizontal);
				robotrandY = randomGenerator.nextInt(vertical);
			}
			else {
				TheRobots temp = new TheRobots(this.robotrandX,this.robotrandY, Direction.getRandomDirection());
				ID = temp.robotid();
				test.add(temp);
				System.out.println("Added robot");
				isEmpty = true;
			}
			
			
		}
		while(!isEmpty);
	}


	public TheRobots getRobotAt(int x, int y) {
		//boolean RobotIsHere = false;
		int keeptrack = 0;
		//creat temp robot
		System.out.println("Size of arraylist is" + test.size());
		for(int i=0; i<test.size(); i++) {
			if(test.get(i).isHere(x, y)){
					return test.get(i);
			}
		}
		return null;
	}
	
	public boolean isNotBiggerThanOrLessThanArena(int x, int y) {
		if(x >= Horizontal-1 || y >= vertical-1 || x < 0 || y < 0) return false;
		return true;
	}
	
	public boolean canMoveHere(int x, int y) {
		if(getRobotAt(x,y) == null && isNotBiggerThanOrLessThanArena(x,y)) {
			System.out.println("No robot here and is not bigger"); 
			return true;
		}
		System.out.println("There is robot here and it is bigger"); 
		return false;
	}
	
	public void showRobots(ConsoleCanvas c) {
		for(int i=0; i<test.size(); i++) {
			TheRobots temp = new TheRobots(test.get(i).getX(), test.get(i).getY(), test.get(i).direction);
			temp.displayRobot(c);
		}
	}
	
	public void moveAllRobot() {
		for(int i=0; i<test.size(); i++) {
			test.get(i).tryToMove(this);
		}
	}
	
	int getX() {
		return Horizontal;
	}
	int getY() {
		return vertical;
	}
	
	public String toString(TheRobots e) {
		return "Arena is of ".concat(Integer.toString(Horizontal) + " by " + Integer.toString(vertical) + " with " + e.toString()); //Robot " + Integer.toString(e.robotid()) + " at position " + e.getX() + " " + e.getY());//this.robotrandY
	}
	
	public String fileString(TheRobots e) {
		return (e.toStringImprovised() + " " + e.getDX() + " " + e.getDY());
	}
	
	public void startUp() {
		
		for(int i=0; i<=15; i++) {
			this.addRobot();
			
		}
	}
	
	public static void main(String[] args) {
		RobotArena a = new RobotArena(20, 10);
		//RobotArena b = new RobotArena(20, 10);
		//a.addRobot();
		//b.addRobot();
		//System.out.println(a.toString());
		//System.out.println(b.toString());
		
		//declare array list
		//String temp[] = {"a","b","c","d","e","f","g","h"};
	
		a.startUp();
		
		System.out.println(test.size());
		for(int i=0; i<test.size(); i++) {
			System.out.println(a.toString(test.get(i)));
		}
	}
}
