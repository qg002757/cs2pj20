package Robot;

public class TheRobots {
	private int x,y,robotID, dx,dy;
	private static int identifier = 0;
	public Direction direction;
	
	public TheRobots (int POSX,int POSY, Direction temp) {
		x = POSX;
		y = POSY;
		direction = temp;
		robotID = identifier++;
		dx = dy = 1;
	}
	
	int getX() {
		return x;
	}
	int getY() {
		return y;
	}
	int getDX() {
		return dx;
	}
	int getDY() {
		return dy;
	}
	int robotid() {
		return robotID;
	}
	
	public String toString() {
		return "Robot of ".concat(Integer.toString(robotID) + " is at position "+ getX() + " " + getY() + " direction is " + direction);
	}
	
	public String toStringImprovised() {
		return getX() + " " + getY();
	}
	
	public boolean isHere(int sx, int sy) {
		//check if X,Y position is free
		if(sx == this.getX() && sy == this.getY()) return true;
		return false;
	}
	
	public void tryToMove(RobotArena a){
		//find the direcition its moving
		if(direction == Direction.NORTH) {
			//can move north
			//now check if the next available position is free
			//For north, we increase just the Y
			if(a.canMoveHere(x, y-dy)) {
				System.out.println("Can move");
				y -=dy;
			}
			else {
				//chnage direction
				System.out.println("Can not move");
				direction = Direction.getNextDirection(direction);
			}
			
		}
		else if(direction == Direction.EAST) {
			//can move east
			
			if(a.canMoveHere(x+dx, y)) {
				x +=dy;
			}
			else {
				//chnage direction
				System.out.println("Can not move");
				direction = Direction.getNextDirection(direction);
			}
		}
		else if(direction == Direction.SOUTH) {
			//can move south
			
			if(a.canMoveHere(x, y+dy)) {
				y +=dy;
			}
			else {
				//chnage direction
				System.out.println("Can not move");
				direction = Direction.getNextDirection(direction);
			}
		}
		else {
			//can move west
			if(a.canMoveHere(x-dx, y)) {
				y +=dy;
			}
			else {
				//chnage direction
				System.out.println("Can not move");
				direction = Direction.getNextDirection(direction);
			}
		}
	}
	
	public void displayRobot(ConsoleCanvas c) {
		c.showIt(this.getX(), this.getY(), 'R');
	}
	
	public static void main(String[] args) {
		TheRobots d = new TheRobots(5, 3, Direction.SOUTH);
		TheRobots e = new TheRobots(10,5,Direction.NORTH);
		System.out.println(d.toString());
		System.out.println(e.toString());
	}
}
