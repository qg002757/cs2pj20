package Robot;

public class ConsoleCanvas {
	private int xSize, ySize;
	private char[][] displayChars;
	
	ConsoleCanvas(Integer xS, Integer yS, String userStr){
		xSize = xS+2; //the extrea 2 is for border
		ySize = yS+2;
		displayChars = new char[xSize][ySize];
		
		padChars(' ','#',userStr);
	}
	
	private void padChars(char pchar, char bchar, String userStr) {
		int topchk = Math.max((xSize-8)/2, 0); //for student num
		
		for(int xct=0; xct<xSize; xct++) {
			for(int yct=0; yct<ySize; yct++) {
				if(xct>0 && xct<xSize-1 && yct>0 && yct<ySize-1) {
					displayChars[xct][yct] = pchar;
				}
				else if(xct>=topchk && xct<8+topchk && yct==0) {
					displayChars[xct][yct] = userStr.charAt(xct-topchk);
				}
				else {
					displayChars[xct][yct] = bchar;
				}
			}
		}
	}
		
		public void showIt(int x, int y, char ch) {
			//get rid of previous
			displayChars[x+1][y+1] = ch;
		}
		
		public void removePrevious(int x, int y, char ch) {
			//get rid of previous
			displayChars[x+1][y+1] = ch;
		}
		
		public String toString() {
			String ans="";
			for(int yct=0; yct<ySize; yct++) {
				for(int xct=0; xct<xSize; xct++) {
					ans+=displayChars[xct][yct];
				}
				ans+='\n';
			}
			return ans;
		}
		
		public static void main(String[] args) {
			ConsoleCanvas c = new ConsoleCanvas(10, 5, "31002757");
			c.showIt(4,3,'R');
			System.out.println(c.toString());
		}
}

