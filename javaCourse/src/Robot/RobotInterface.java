package Robot;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import javax.swing.JFileChooser;
import java.io.FileWriter;

public class RobotInterface {
	private Scanner s;
	private static RobotArena myArena = new RobotArena(20, 6);
	private static Scanner userInput;
	private static String userResponse;
	//Above is sfor user input so if they want to specify the size themselve
	
	private static Scanner specifySizeX;
	private static Scanner specifySizeY;
	private static Integer dimX; //using the integer wrapper
	private static Integer dimY;
	private static ConsoleCanvas test;
	private String fileContent;
	private String nameOfFile;
	private static int X = myArena.getX();
	private static int Y = myArena.getY();
	public int robotsNum = 1; //since first we will have 1 robot
public RobotInterface(int x, int y, ConsoleCanvas test, boolean addInitialRobot) { //have default values for constructors
	s = new Scanner(System.in);
	boolean running = true;
	char ch = ' ';
	if(addInitialRobot) {
		test.showIt(x/2, y/2, 'R');
		TheRobots temp = new TheRobots(x/2,y/2, Direction.getRandomDirection());
		myArena.test.add(temp);
	}
	else {
		robotsNum = 0;
	}
	this.doDisplay(test);
	do {
		System.out.print("Enter (A)dd Robot, (M)ove, (D)isplay, (S)imulate, (I)nformation, (R)ead from file, (F)Save to file, or e(X)it >");
		ch = s.next().charAt(0);
		s.nextLine();
		switch(ch) {
			case 'M':
			case 'm':
				//get rid of previous
				for(int i=0; i<myArena.test.size(); i++) {
					test.removePrevious(myArena.test.get(i).getX(), myArena.test.get(i).getY(), ' ');
				}
				myArena.moveAllRobot();
				for(int i=0; i<myArena.test.size(); i++) {
					test.showIt(myArena.test.get(i).getX(), myArena.test.get(i).getY(), 'R');
				}
				break;
				
			case 'S':
			case 's':
				simulate(10,test);
			case 'D':
			case 'd':
				doDisplay(test);
				break;
		
			case 'A':
			case 'a':
				myArena.addRobot();
				test.showIt(myArena.test.get(robotsNum).getX(), myArena.test.get(robotsNum).getY(), 'R');
				robotsNum+=1;
				break;
			case 'I':
			case 'i':
				for(int i=0; i<myArena.test.size(); i++) {
					System.out.println(myArena.toString(myArena.test.get(i)));
				}
				if(myArena.test.size() == 0) {
					System.out.println("Size is 0");
				}
				break;
			case 'F':
			case 'f':
				String Append = myArena.getX() + " " + myArena.getY();
				for(int i=0; i<myArena.test.size(); i++) {
					Append += ";"+ myArena.fileString(myArena.test.get(i));
				}
				System.out.println("Test");
				//System.out.println(Append); //testing
				TextFile tf = new TextFile("Text files", "txt");		// create object looking for *.txt Text files
				Scanner nameOfFile;
				nameOfFile = new Scanner(System.in);
				System.out.println("Enter the nmae of your new file");
				nameOfFile.nextLine();
				try {
					File myObj = new File(""+nameOfFile);
					if(myObj.createNewFile()) {
						System.out.println("File created");
						FileWriter myWriter = new FileWriter(""+nameOfFile);
						myWriter.write(Append);
						myWriter.close();
						System.out.println("Written to your file");
					}
					else{
							System.out.println("Counld not be created");
						}
				}catch (IOException e) {
					System.out.println("An error occured");
					e.printStackTrace();
				}
				/*if(tf.createFile()) {
					//System.out.println("Success");
				}
				/*
				if (tf.openFile()) {									// open file
					System.out.println("Reading from " + tf.usedFileName());
					System.out.println(tf.readAllFile());				// read whole file into str which is printed to console
					
	/*				while (tf.getNextline())							// while there is a line to read
						System.out.println(tf.nextLine());				// get it and output to console
					tf.closeFile();										// close file
	
				}
				else System.out.println("No read file selected");
				System.out.println("Press \"r\" or \"R\" to read from file");
				*/
				break;
			case 'R':
			case 'r':
				//if(tf.openFile())
					//fileContent = tf.readAllFile();
			//fileContent = fileContent.substring(0,fileContent.length()-1);
				System.out.println("Testing");
				break;
			//we are here
			case 'X':
			case 'x':
				running = false;
				break;
			}
}
while(running); //ch !='X' || ch != 'x'
s.close();
}

void doDisplay(ConsoleCanvas test) {
	System.out.println(test.toString());
	}


private void simulate(int numTimes, ConsoleCanvas test) {
	for(int ct=0; ct<numTimes; ct++) {
		for(int i=0; i<myArena.test.size(); i++) {
			test.removePrevious(myArena.test.get(i).getX(), myArena.test.get(i).getY(), ' ');
		}
		myArena.moveAllRobot();
		for(int i=0; i<myArena.test.size(); i++) {
			test.showIt(myArena.test.get(i).getX(), myArena.test.get(i).getY(), 'R');
		}
		doDisplay(test);
		try {
			TimeUnit.MILLISECONDS.sleep(200);
		} catch(Exception e) { //InteruptedException
			e.printStackTrace();
		}
	}
}

	
	public static void main(String[] args) {
		/*
		 * So we need to allow the user to define their own area size
		 * i will use an if statement
		 */
		userInput = new Scanner(System.in);
		System.out.println("Enter \"U\" if you want to be able to manlly include the dimension, or \"A\" if you want it to have the defualt");
		userResponse = userInput.nextLine();
		if(userResponse.equals("A")) {
			test = new ConsoleCanvas(X,Y,"31002757");
			RobotInterface r = new RobotInterface(X,Y,test,true); //make it true so initial robot will be created
			r.doDisplay(test);
		}
		else if (userResponse.equals("U")){
			//Allow use to specify the size of the dimension
			//System.out.println("You have picked to add the size yourself");
			specifySizeX = new Scanner(System.in);
			specifySizeY = new Scanner(System.in);
			System.out.println("Enter the X dimension of the arena");
			dimX = Integer.parseInt(specifySizeX.nextLine());
			System.out.println("Enter Y dimension of the arena");
			dimY = Integer.parseInt(specifySizeY.nextLine());
			
			//validate inputs (check if they are numbers)
			if((dimX instanceof Integer) ==  false || (dimY instanceof Integer) == false) {
				System.out.println("Numbers provided are not integers");
			}
			else {
				test = new ConsoleCanvas(dimX,dimY,"31002757");
				//create arena with the inputs provided
				RobotInterface r = new RobotInterface(dimX,dimY,test,false); //make it false so arena will be empty
				r.doDisplay(test);
			}
		
		}
		else {
			System.out.println("Entry un-recognized");
		}
	}
}

