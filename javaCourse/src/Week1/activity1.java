//Hello world

package Week1;
public class activity1 {
	public static void main(String []args) {
		System.out.println("Hello world");
}
}

//Notes

/*
What is system.out
Processes in modern OS (and for that matter,
several older OS) have three standard streams
associated with them:
1.) Standard in - Stream-based input (stdin)
2.) Standard Out - Stream-based output (stdout)
3.) Standard error - Stream based error output (stderr)

Collectively they are called the standard streams
source: https://stackoverflow.com/questions/32287816/what-is-system-out-exactly

JAR files - These is actually a ZIP file,
It can contain anything - usually it contains
compiled Java code (*.class), but sometimes also
Java SourceCode (*.Java).
Source: https://stackoverflow.com/questions/12079230/what-exactly-does-a-jar-file-contain

*/