package Week1;
//Generate someone's initial
//For example, given:
/*
 * 
 * John Smith Alan,
 * initiail will be JSA
 * 
 */


import javax.swing.*;
public class activity3 {
	public static String compute(String user) {
		if(user.indexOf(" ") == -1) {
			System.out.println("No more");
			return user.substring(0,0);
		}
		return user.substring(user.indexOf(" ")+1,(user.indexOf(" ")+1)+1) + compute(user.substring(user.indexOf(" ")+1));
	}
	
	public static int computeAmend(String user, String cutter) {
		//find index of space-character, and use substring to return
		if(user.indexOf(" ") != -1) return Integer.parseInt(user.substring(0,user.indexOf(cutter)));
		return Integer.parseInt(user);
	}
	
	public static void main(String [] args) {
	String user = JOptionPane.showInputDialog(null, "Please enter you full name");
	String Initial = user.substring(0,1);
	Initial+= compute(user); //Using recursion for task
	/*
	/*
	 * Steps:
	 * 1.) Store valaue of first index
	 * 2.) Loop through and find empty space then store current+1
	 * 3.) end
	 
	//This version is much simpler than the above one of using recursion
	Initials+=user.charAt(0);
	for(int i=0; i<user.length();i++) {
		if(user.charAt(i) == ' ') {
			//DO error checking - see if next character is also empty, then tell users
			Initials+=user.charAt(i+1);
		}
	}
	
	
	
	//Final alternative version is to use a nest of each method
	 *Use a foor loop to go over the string, and count the number of space
	 *Then use the indexOf to get the value at the space index and add them up using substring
	 *String Initials;
	 *for(int i =0; i<user.length(); i++){
	 *	if(i == 0){
	 *		Initials +=user.substring(0,1);
	 *}
	 *if(user.charAt(i) == ' '){
	 *	Initials+=user.substring(user.indexOf(i)+1,(user.indexOf(i)+1)+1)
	 *}
	 *
	 *
	*/
	JOptionPane.showMessageDialog(null, "Your Initials is " + Initial);
	
	//Alternatively, we could have used the substring method
	//for example:
	/*
	 * Given user name, e.g. Same John
	 * Use substring(0,1) -> gets the first initial
	 * Use example = indexOf(" ") -> gets first instace of space
	 * Use substring(example+1,(example+1)+1) -> Gets seconds intiial after space
	 * Do this till no more space
	 * 
	 * Im not sure, this it seems this can be done using recursion -> YES, IT CAN BE DONE USING RECURSION
	 */
	/*
	*/
	
}
}
