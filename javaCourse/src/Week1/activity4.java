package Week1;

import javax.swing.*;
public class activity4 {
	public static void main(String [] args) {
//Allow user to enter multiple string seperated with space and then put them into an array
	String user = JOptionPane.showInputDialog(null, "Enter lists of strings");
	//declare array
	int spaces = 1; //For last character
	for(int i=0; i<user.length(); i++) {
		if(user.charAt(i) == ' ') {
			spaces +=1;
		}
	}
	String [] userString = new String[spaces];
	
	for(int i =0; i<spaces-1; i++) {
		userString[i] = user.substring(0, user.indexOf(" "));
		user = user.substring(user.indexOf(" ")+1);
	}
	userString[spaces-1] = user;
	
	for(int i=0; i<spaces; i++) {
		System.out.println(userString[i]);
	}
	
}
}
