package Week1;

/*Allow users to enter a string and report 
the number of s in the string*/
import javax.swing.*;
public class activity2 {
	public static void main(String [] args) {
		int numOfIntegers = 0;
		String user = JOptionPane.showInputDialog(null, "Please enter a string");
		//JOptionPane.showMessageDialog(null, "The string you entered is " + "\"" +user+"\"");
		//Comment the above and move it down to displa number of s's in string
		//Could use either for or while loop for this
		//Same speed, so lets use for
		for(int i=0; i<user.length(); ++i) { //in for loop pre-increment/post-increment will not change the finctionality
			if(user.charAt(i) == 's' || user.charAt(i) == 'S') { //Check for both lowercase and uppercase 
				//Another implementation is compare the number, since a char is an int in disguise
				numOfIntegers+=1;
			}
			//No need for a else statement, only the if wil run if the character is s
		}
		JOptionPane.showMessageDialog(null, "The number of s's in this string is " + numOfIntegers);
	}
}

//Notes
/*
Creating a string object is done using the 
String keyword, e.g.
String Mystring

String is not a primitive datatype. Primitive 
datatype in Java include ints, floats, char etc

If you want, you can declare a string, and not 
assign it, ow what this whill do is allocate space
in memory for it, and then we can create a string object and
the varibale we previosuly created can refer to the 
string object created.

An example is:
String   name;
name  =  new  String("Java VM");

String in Java is just like in C, they are just
a series of charcater and we can refer to their
index, but we use the charAt() methpod and add the index we want

For example:
String text;
text = "Espresso" //Now this is just like text = new string("Expresso")
text.CharAt(2) -> Results in the character p
E s p r e s s o
1 2 3 4 5 6 7 8

To get the length of a string, we use the length()
Since string is an object, we have properties such as length, charAt() etc

String str1, str2, str3;
str1 = "Hello";
str2 = ""; //empty string
str3 = " "; //one space

str1.length() -> 5
str2.length() -> 0
str3.length() -> 1 //Java trats space charcater or whitespace character as a normal charatcer if you get wat i mean
*/