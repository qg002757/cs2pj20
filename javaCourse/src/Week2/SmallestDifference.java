package Week2;
import javax.swing.*;
import java.lang.*;
public class SmallestDifference {
	private int[] numbers;
	
	SmallestDifference(String instr){
		StringSplitter S = new StringSplitter(instr, " ");
		numbers = S.toIntegers();
	}
	
	private void swap(int a, int b) {
		int temp = a;
		a = b;
		b = temp;
	}
	
	private void sort(int a, int b) {
		if(a > b) {
			swap(a,b);
		}
	}
	public int FindSmallestSorted() {
		//First lets sort the numbers out
		for(int i=0; i<numbers.length-1; i++) {
			sort(i, i+1);
		}
		int res;
		int KeepTracKofSmallest = Math.abs(numbers[0] - numbers[1]);
		for(int i=0; i<numbers.length-1;i++) {
			res = Math.abs(numbers[i] - numbers[i+1]);
			if(i !=0) {
				if(res < KeepTracKofSmallest) {
					KeepTracKofSmallest = res;
				}
			}
		}
		return KeepTracKofSmallest;
	}
	
	public String toString() {
		return "Smallest difference is ";
	}
	
	public static void main(String[] args) {
		String userIn = JOptionPane.showInputDialog(null, "Enter series of numbers seperated by space > ");
		SmallestDifference sd = new SmallestDifference(userIn);
		//sd.FindSmallestSorted();
		JOptionPane.showMessageDialog(null, sd.toString()+sd.FindSmallestSorted() + "at index");
	}
}

