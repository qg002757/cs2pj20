package Week2;
public class MultiplicationTable{
	private int maxNum;
	private int[][] TableData;
	
	MultiplicationTable(int maxN){
		maxNum = maxN;
		TableData = new int[maxN+1][maxN+1];
	}
	
	private void makeTable() {
		for(int i=1; i<=maxNum; i++) {
			for(int j=1; j<=maxNum;j++) {
				TableData[i][j] = i*j;
			}
		}
	}
	
	public String toString() {
		String res = "Benjamin Multiplication Table" + "\n";
		for(int i=1; i<=maxNum; i++) {
			for(int j=1; j<=maxNum; j++) {
				res+=TableData[i][j];
				res +="\t";
			}
			res +="\n";
		}
		return res;
	}
	
	public static void main(String[] args) {
		MultiplicationTable mt = new MultiplicationTable(10);
		mt.makeTable();
		System.out.print(mt.toString());
	}
}