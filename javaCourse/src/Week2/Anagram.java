package Week2;
import javax.swing.*;
import java.util.Scanner;

public class Anagram {
	String Word1;
	Anagram(String word1){
		Word1 = word1;
	}
	private int StringSplitter() {
		//Since there will only be 2 ; then it is rather simple
		int findSemiColon = Word1.indexOf(';');
		String FirstPart = Word1.substring(0,findSemiColon-1).toLowerCase();
		String SecondPart = Word1.substring(findSemiColon+2, Word1.length()).toLowerCase();
		//first check length to see they are of the same length
		if(FirstPart.length() != SecondPart.length()) {
			System.out.println(SecondPart.length());
			System.out.println(FirstPart.length());
			return 0;
		}
		else {
			//Testing
			System.out.println(SecondPart.length());
			System.out.println(FirstPart.length());
		}
		int KeepTrack=0;
		for(int i=0; i<FirstPart.length(); i++) {
			for(int j=0; j<SecondPart.length(); j++) {
				if(FirstPart.charAt(i) == SecondPart.charAt(j)) {
					KeepTrack+=1;
					System.out.println(KeepTrack);
					break;
				}
				
			}
		}
		if(KeepTrack != FirstPart.length()) {
			return 0;
		}
		return 1;
	}
	//main
	public static void main(String[] args) {
		String userIn = JOptionPane.showInputDialog(null, "Enter string seperated by ; Note: Add space before and after ;");
		Anagram Test = new Anagram(userIn);
		int findSemiColon = userIn.indexOf(';');
		String FirstPart = userIn.substring(0,findSemiColon).toLowerCase();
		String SecondPart = userIn.substring(findSemiColon+1, userIn.length()).toLowerCase();
		if(Test.StringSplitter() == 1) {
			JOptionPane.showMessageDialog(null, FirstPart + "and" + SecondPart + " are anagrams");
		}
		else {
			JOptionPane.showMessageDialog(null, FirstPart + "and" + SecondPart + " are not anagrams");
		}
	}
}
