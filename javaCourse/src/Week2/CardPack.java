package Week2;
import java.util.*;

public class CardPack {
	private Card[] thePack;
	
	public CardPack(int maxCard, Random randGen) {
		thePack = new Card[maxCard];
		
		for(int ct=0; ct<maxCard; ct++) {
			thePack[ct] = new Card(randGen);
		}
		
	}
	public String toString(String mess) {
		String s = mess + " : ";
		for(int ct=0; ct<thePack.length;ct++) {
			s +=thePack[ct].toString()+" ";
		}
		return s;
	}
	
	public static void main(String[] args) {
		Random randGen = new Random();
		
		CardPack p = new CardPack(20,randGen);
		System.out.println(p.toString("Unsorted"));
		p.swapCards(5, 10);
		System.out.println(p.toString("Swapped "));
		p.bubbleSort();
		System.out.println(p.toString(" Sorted"));
	}
	
	private void swapCards(int c1, int c2) {
		Card temp = thePack[c1];
		thePack[c1] = thePack[c2];
		thePack[c2] = temp;
	}
	
	public void bubbleSort() {
		for(int ct1=0; ct1<thePack.length-1; ct1++) {
			for(int ct2=thePack.length-1; ct2>ct1; ct2--) {
				if(thePack[ct2-1].compareCard(thePack[ct2]) < 0) {
					swapCards(ct2-1,ct2);
				}
			}
		}
	}
}
