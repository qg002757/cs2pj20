package Week2;

public class ExampleProgram {
	private int distanceTravelled=0;
	ExampleProgram(){
		distanceTravelled=0;
	}
	
	public void move(int speed, int time) {
		distanceTravelled+=speed*time;
	}
	
	public String toString() {
		return "car has travelled " + distanceTravelled;
	}
	
	public static void main(String[] args) {
		ExampleProgram myCar = new ExampleProgram();
		myCar.move(5,5);
		myCar.move(15, 10);
		myCar.move(30,15);
		System.out.println(myCar.toString());
		;
	}
}
