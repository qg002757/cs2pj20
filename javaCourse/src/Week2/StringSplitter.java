package Week2;

import java.util.Arrays;

public class StringSplitter {

	String Value;
	String Seperator;
	public StringSplitter(String value, String seperator){
		Value = value;
		Seperator = seperator;
	}
	
	
	private String[] toStrings() {
		int CountSpaces = 0;
		for(int i=0; i<Value.length(); i++) {
			if(Value.charAt(i) != ' ' ) {
				CountSpaces+=1;
			}
		}
		String[] res = new String[CountSpaces];
		int KeepTrack=0;
		for(int i=0; i<Value.length(); i++) {
			if(Value.charAt(i) != ' ' ) {
				res[KeepTrack] = "\"" + Value.charAt(i) + "\"";
				KeepTrack+=1;
			}
		}
		//return res;
		return Arrays.copyOf(res,res.length);
	}
	
	public int[] toIntegers() {
		int CountSpaces = 0;
		for(int i=0; i<Value.length(); i++) {
			if(Value.charAt(i) != ' ' ) {
				CountSpaces+=1;
			}
		}
		int[] res = new int[CountSpaces];
		int KeepTrack=0;
		for(int i=0; i<Value.length(); i++) {
			if(Value.charAt(i) != ' ' ) {
				res[KeepTrack] = Value.charAt(i);
				KeepTrack+=1;
			}
		}
		//return res;
		return Arrays.copyOf(res,res.length);
	}
	//main
	public static void main(String[] args) {
		StringSplitter ME = new StringSplitter("2 5 6 9", " ");
		System.out.println(ME.toString());
		
		String[] temp = ME.toStrings();
		
		for(int ct=0; ct<temp.length; ct++) {
			System.out.println(temp[ct] + "\t");
		}
		System.out.println();
		
		temp[0] = "fred";
		
		for(int ct=0; ct<temp.length; ct++) {
			System.out.println(temp[ct] + "\t");
		}
		System.out.println();
		
		System.out.println(ME.toString());
	}
}
