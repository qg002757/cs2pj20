package Week2;
import java.util.*;
public class Card {
	public char cardSuit;
	public int cardVal;
	
	public Card(Random randGen) {
		String suitStr = "HCDS";
		
		cardVal = 1+randGen.nextInt(13);
		//Gets random number between 1 and 13
		cardSuit = suitStr.charAt(randGen.nextInt(4));
		//0 and 3
	}
	
	public String toString() {
		String cardValStr = " A23456789TJQK";
		char cVCH = cardValStr.charAt(cardVal);
		
		return String.valueOf(cVCH)+ String.valueOf(cardSuit); //returns something like AJ
	//valueOf() returns string representation of a non-string
	}
	
	public int compareCard(Card other) {
		if(other.cardSuit < cardSuit) {
			return -1;
		}
		else if (other.cardSuit > cardSuit) {
			return 1;
		}
		else if(other.cardVal == cardVal) {
			return 0;
		}
		else if(other.cardVal < cardVal) {
			return -1;
		}
		else {
			return 1;
		}
	}

public static void main(String[] args) {
	Random rGen = new Random();
	Card c = new Card(rGen);
	Card c2 = new Card(rGen);
	int cf = c.compareCard(c2);
	String cfstr = "<=>";
	System.out.println("Random card is " + c.toString());
	System.out.println(c.toString() + " " + cfstr.charAt(cf+1) + " " + c2.toString());
}
}