package Week3;
import Week1.*;
public class BallPark {
	private int xmax,ymax;
	private Ball b;
	public BallPark(String val) {
		activity3 ss = new activity3();
		xmax = ss.computeAmend(val, ";");
		ymax = ss.computeAmend(val.substring(val.indexOf(";")).trim(), ";");
		//b = new Ball(ss.computeAmend(val, val))) You left off here
	}
	public void movePark() {
		b.moveBall(this);
	}
	
	public int getXSize() {
		return xmax;
	}
	public int getYSize() {
		return ymax;
	}
	
	public String toString() {
		return "Arena size is of ".concat(Integer.toString(getXSize()) + " by " + Integer.toString(getYSize()) + " with " + b.toString());
	}
	
	
	public int canGoHere(int x, int y) {
		int status = 0;
		if(x<0 || x>=xmax) status+=1; //So position can be be less than 0(orgin of map) and greater than map
		if(y<0 || y>ymax) status +=2;
		return status;
	}
	
	public static void main(String[] args) {
		BallPark bp = new BallPark("20 12;16 9");
		System.out.println(bp.toString());
		for(int ct=0; ct<10; ct++) {
			bp.movePark();
			System.out.println(bp.toString());
		}
	}
	
}
