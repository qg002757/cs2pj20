package Week3;
import Week1.*;

public class Ball {
	private int x,y,ballID,dx,dy;
	private static int identifier = 0;
	public Ball(int ballX,int ballY) {
		/*
		 *Increase identifier variable every-time
		 *ball object is created 
		 */
		ballID = identifier++; //post-incremrent
		x = ballX;
		y = ballY;
		
		dx = 1; //velocity in X
		dy = 1; //velocity in Y
	}
	
	public Ball(String s) {
		this(0,0);
		activity3 ss = new activity3();
		setX(ss.computeAmend(s, " "));
		//System.out.println(s.substring(s.indexOf(" ")).trim());
		setY(ss.computeAmend(s.substring(s.indexOf(" ")).trim(), " "));
		}
	//get values for X and Y
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	
	public void setX(int value) {
		x = value;
	}
	public void setY(int value) {
		y = value;
	}
	
	public String toString() {
		return "Ball of ".concat(Integer.toString(ballID) + " is at position "+ getX() + " " + getY());
	}
	
	public void moveBall(BallPark bp) {
		int newPosX = x + dx; //move ball horizontally by 1
		int newPosY = y + dy;
		switch(bp.canGoHere(newPosX,newPosY)) {
		case 0 : 
			x = newPosX;
			y = newPosY;
			break;
		case 1:
			dx = -dx; //ball reach end of right
			break;
		case 2:
			dy = -dy;
			break;
		case 3:
			dx = -dx;
			dy = -dy;
			break;
		}
	}
	
	
	public static void main(String[] args) {
		Ball b = new Ball("5 3");
		System.out.println(b.toString());
		Ball b2 = new Ball(8,12);
		System.out.println(b2.toString());
	}
}
