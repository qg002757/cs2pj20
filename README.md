# CS2PJ20
___
Worth 20 credits
# Prequisite
___
+ C/C++
+ Data - Variables
+ Control statements
+ Functions
+ Libraries
+ Comments
+ Top Down/Bottom Up design
+ OOP
+ Debugging and Problem solving
# Comparision between C/C++ and Java
| Metrics                       | C                   | C++                               | Java                |
|-------------------------------|---------------------|-----------------------------------|---------------------|
| Programming Paradigm          | Procedural language | Object-Oriented Porgramming (OOP) | Pure Object Oriented|
| Orgin                         |                     |                                   |                     |
| Translator                    |                     |                                   |                     |
| Platform Dependency           |                     |                                   |                     | 
| Code Execution                |                     |                                   |                     |
| Approach                      |                     |                                   |                     |
| File generation               |                     |                                   |                     |
| Pre-processor directives      |                     |                                   |                     |
| Keywords                      |                     |                                   |                     |
| Datatypes (Union, structures) |                     |                                   |                     |
| Inheritance                   |                     |                                   |                     |
| Overloading                   |                     |                                   |                     |
| Pointers                      |                     |                                   |                     |
| Allocation                    |                     |                                   |                     |
| Exception handling            |                     |                                   |                     |
| Templates                     |                     |                                   |                     |

[Resource](https://www.edureka.co/blog/difference-between-c-c-and-java)

# Coursework
Coursework in part 1 and part 2 (For part 2, **creativity** gives more marks)
100% coursework
1. Autumn term :
* Online test 1 (10%) - Checking knowledge of OOP (week 7)
* Coursework 1 Java assignment (40%)
2. Spring term:
* Coursework 2 Android assignments (50%)

# Tools
1. IDE - Preferably Eclipse
2. JDK - Java development kit

# Simple 4 programs to-do
1. Should be able to know how to write a Hello world in Java
2. Count the number of s's in string
3. Determine someone's initial from string
4. Count number of names in string

# JOptionPane
[The JOptionPane class is used to provide standanrd dialog boxes such as message dialog box, confirm dialog box and input dialog box. These dialog boxes are used to display information or get input from the user. The JOptionPane class inherits JComponent class](https://www.javatpoint.com/java-joptionpane)

# Weekly Tasks
These weekly task as to be done every week

# C, C++, Java
C is an imperative (Meaning you must specific everthing little detail) language
Algorithms + Data structure = programs
C is C bodged for OOP
Java is true OOP, you start inheritenly thinking about objects. So if you are going to solve a problem you are going to need some data and you are going to need to process the data. And the data and the finctions that process the data are all in one entity. So it was designed specifically for OOP
An example of C code is:
''' c
    #include <stdio.h>
    int main(int argc, char * argv[]){
        printf("Hello world");
    }
'''
**main is the entry point for a C program**
The above is similiar for a C++ program, with C++, you can create classes

# Organization of C program
C programs are organized in a tree like structure if you like, so it looks something like:

'''
root

|

|--src

    |

    |-----header

__  |

|

|--bin

|

|--Makefile

'''

Some people like to seperate their header file 

If you are using MS VS, then you will have a .sln (This is MS solution file that contains your project)

C++ is organized in a similar way

Now Java is a bit different. Now it is an OOP language, and it is built in the idea that you will have data and the assoiated functions in one entity. Your files that you create or your program will consist of one or more java source files. Inisde the source file is a lcass, and that defines a type of object. Withint he class, you deinf ethe varibales and functions that process those varibales.
The class file will need to have a main funciton, and that is the function that is called when the program runs. You cna have multiple main function depending on what you are doing.

If you want to use another class that you have written or is in the library, you need to import it. So the import tells the system to look at the class and it can find out what functions are available and then you can use them.

# Workspace
Eclipse uses the concept of workspace. Workspace can ave multiple Java programs if you want. For example, when using MsVS, it create something similar to a workspace for us, when when i go to that workspace dirctory, i will see:
<img src='/Images/image.png' alt='MsVS workspace'/>
So this is jsut the same concept for Eclipse when using java
You should know the meaning of folders such as:
+ bin
+ src
+ settings

# Package
Now say for example, you decide to write more than 1 programs, you want to have differnte areas for each, so within the project, you create a package. A package is a collection of source files associated with one or more program.
So what you can do if you want to create multiple program is:
create workspace -> create project -> create a package
Now inside of a package, you can have one or more classes. You can have multiple source files within a package.
Now if you want to run a specific program within a package, then Eclipse will run the main associated with that class in the program you want to run.
Later you will have more advanced structure such as a sereis of classes, java file, and one will have the main whihc will call some of the other. Now when you want to use something in anothe class, you just need to import it. The import is just like the **preprocessor** statment '#include' in C/C++
A simple program is as follows:
<img src='/Images/image2.png' alt='First simple program'>
So this can be in the file helloWorld.java
At the top, you can see whihc packaage it is in, so **package week1**

Another example code we have, but this time including a library is:
<img src='/Images/image3.png' alt='Second simple program using library'>

**Notice that the package comes before the import**

# Access modifiers
There are several access modifiers in Java such as Public
# Static
Static means can be used in varibales and function. For varibales, this will make the varibale exist throughout the existence of the program. Even if the varibale is declared in a function (as we know varibale functions are created when the function is invoked) the varibale will still remain in existence even if the function cease to exist

# Comments
Comments are pretty stringht forward

# Notes
<img src='/Images/image4.png' alt='Some Notes to keep'>

# Virtual machines
Java code needs to be compiled, but how javas does it, is different to how C/C++ does it. So if you have a compiled language like C, then you can write code for differnent computers that have dofferten OS, e.g.
<img src='/Images/image5.png' alt='Image5'>
So you take the code, the compiler generates the binary and this binary can be understood by the computer

In contrast, we have an intepreterd langaueg such as python, e.g.
<img src='/Images/image6.png' alt='Image 6'>

Now for Java it is differnte, because you start with source code and you have compiler, but what they generate is some intermediate so called Java bytecode, and then for each of the differnte computer, there is a virtual machine that understand javas byte code and turns it into runnable code, e.g.
<img src='/Images/image7.png' alt='Image7'>

Now the process of how this work is that your computer OS, will be urnning various services/applications and one of these will be the Java virtual machine, e.g.
<img src='/Images/image8.png' alt='Image 8'>
and that java virtual machine can run one of these java application of the assoicated bytecode. So you might have lots of diffete java program, but only one is running on the OS. The virtual machine is calling the byte code for your particular program. Now there are also Applets, and these are java programs that run under a web browser. So you have again your Computer with its OS running and the dffernte program that it is running, but you also have a web browser whihc itself has a java virtual machine whic runs your java.
<img src='/Images/image9.png' alt='image 9'>

Java can produce:
1. Applications
2. Applets
Servlets, Add-on modules

# Platforms
There is a huge amount of Java platform
<img src='/Images/image10.png' alt='Image 10'>