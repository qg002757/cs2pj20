package ballGUI;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.stage.Stage;
import java.lang.*;

public class BallAnimation extends Application {
	private int xCanvasSize=400, yCanvasSize=500;
	private MyCanvas mc;
	private OneBallWorld obw;
	
	@Override
	public void start(Stage stagePrimary) throws Exception{
		stagePrimary.setTitle("RJMs Ball Animcation");
		
		Group root = new Group();
		Scene scene = new Scene(root);
		stagePrimary.setScene(scene);
		
		Canvas canvas = new Canvas(xCanvasSize, yCanvasSize);
		
		root.getChildren().add(canvas);
		
		mc = new MyCanvas(canvas.getGraphicsContext2D(), xCanvasSize, yCanvasSize);
		
		obw = new OneBallWorld();
		
		new AnimationTimer() {
			public void handle(long currentNanoTime) {
				obw.updateWorld(mc);
				obw.drawWorld(mc);
			}
			}.start();
			
		stagePrimary.show();
		}
		
		
	public static void main(String[] args) {
		launch();
	}
		
		
}

