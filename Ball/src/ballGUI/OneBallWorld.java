package ballGUI;

public class OneBallWorld {
	private double ballX, ballY, ballRad, ballAngle, ballSpeed;
	
	public OneBallWorld(){
		ballX = 100;
		ballY = 180;
		ballRad = 20;
		ballAngle = 45;
		ballSpeed = 5;
	}
	
	public void checkBall(MyCanvas mc) {
		if(ballX < ballRad || ballX> mc.getXCanvasSize() - ballRad) ballAngle = 180 - ballAngle;
		if(ballY < ballRad || ballY > mc.getYCanvasSize() - ballRad) ballAngle = -ballAngle;
	}
	
	public void adjustBall() {
		double radAngle = ballAngle*Math.PI/180;
		ballX += ballSpeed * Math.cos(radAngle);
		ballY += ballSpeed * Math.sin(radAngle);
	}
	
	public void updateWorld(MyCanvas mc) {
		checkBall(mc);
		adjustBall();
	}
	
	public void drawWorld(MyCanvas mc) {
		mc.clearCanvas();
		mc.showCircle(ballX,ballY,ballRad, 'r');
	}
	
	public String toString() {
		return "Ball at " + Math.round(ballX) + ", " + Math.round(ballY);
 	}
}
