package application;
//Here will be the main class where our program should run
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.layout.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import javafx.scene.canvas.*;

public class Main extends Application {
	private int CanvasX = 600,CanvasY = 400;
	Canvas drawToCanvas = new Canvas(CanvasX,CanvasY); //this is to draw
	RobotCanvas instance = new RobotCanvas(drawToCanvas.getGraphicsContext2D(),CanvasX,CanvasY);
	
	private Scene StartUpScene, MainScene;
	private Stage Scene1;
	private Button btn;
	StackPane root;
	public void start(Stage primaryStage) throws Exception{
		Scene1 = primaryStage;
		Scene1.setTitle("StartUp Screen");
		StartUpScene = SceneCreation1();
		MainScene = SceneCreation2();
		
		Scene1.setScene(StartUpScene);
		Scene1.show();
	}
	
	private Scene SceneCreation1() {
		root = new StackPane();
		btn = new Button("Click to start simulation");
		btn.setOnAction(e->SwitchScenes(MainScene));
		root.getChildren().add(btn);
		StartUpScene = new Scene(root,instance.ReturnCanvasX(),instance.ReturnCanvasY());
		return StartUpScene;
	}
	
	private Scene SceneCreation2() {
		root = new StackPane();
		MenBar createMenuBar = new MenBar();
		VBox vb = new VBox(createMenuBar.GetMenuBar());
		root.getChildren().addAll(vb);	
		root.getChildren().add(drawToCanvas);
		MainScene = new Scene(root,instance.ReturnCanvasX(),instance.ReturnCanvasY());
		return MainScene;
	}
	
	private void SwitchScenes(Scene para) {
		Scene1.setScene(para);
		Scene1.setTitle("Robots Simulation");
	}
	
	//So all the above was for the functionality for switching scenes
	
	public static void main(String args) {
		Application.launch(args);
	}

}
