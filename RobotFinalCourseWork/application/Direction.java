package application;

import java.util.Random;

public enum Direction{
	NORTH, EAST, SOUTH,WEST;
	
	public static Direction getRandomDirection() {
		Random rand = new Random();
		return values()[rand.nextInt(values().length)];
	} //values() is a built in method for enums whihc returns an array of all enum constants

	public static Direction getNextDirection(Direction next) {
		//return next direction (so if current is east, reutrn south)
		if(next == Direction.NORTH)return EAST;
		else if(next == Direction.EAST) return SOUTH;
		else if (next == Direction.SOUTH) return WEST;
		else return NORTH;
	}
}