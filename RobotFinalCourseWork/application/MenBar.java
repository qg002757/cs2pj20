package application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
//import javafx.scene.control.Alert;
//Add some java docs here
public class MenBar{
	private Menu m1, m2;
	private MenuItem m1Extended, m2Extended;
	private MenuBar mb;
	
	public MenBar() {
		m1 = new Menu("File explorer");
		m2 = new Menu("More");
		//For our exit button
	
		m1Extended = new MenuItem("Open File");
		m2Extended = new MenuItem("Exit");
		
		m2Extended.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {
		        System.exit(0);						// quit program
		    }
		});
		//MenuItem m2Extend = new MenuItem("Save File");
	
		m1.getItems().add(m1Extended);
		m2.getItems().add(m2Extended);
		mb = new MenuBar();	
		mb.getMenus().add(m1);
		mb.getMenus().add(m2);
	}
	
	public MenuBar GetMenuBar() {
		return mb;
	}
}
