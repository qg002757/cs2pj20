package application;
import java.util.Random;

import javafx.scene.canvas.*;
import javafx.scene.image.Image;

public class RobotCanvas {
	int CanvasX,CanvasY;
	GraphicsContext GraphicsDrawer;
	Random randomGenerator = new Random();
	
	//constructor
	
	public RobotCanvas(GraphicsContext temp, int width, int height) {
		GraphicsDrawer = temp;
		CanvasX = width;
		CanvasY = height;
	}
	
	public int ReturnCanvasX() {
		return CanvasX;
	}
	
	public int ReturnCanvasY() {
		return CanvasY;
	}
	
	public void clearCanvas() {
		GraphicsDrawer.clearRect(0, 0, CanvasY, CanvasX);//clear canvas
    }
	
	public boolean isNotBiggerThanOrLessThanCanvas(double x, double y) {
		if(x >= CanvasX-1 || y >= CanvasY-1 || x < 0 || y < 0) return false;
		return true;
	}
	
	public void drawEntity (Image i, double x, double y, double sz) {
		//We can use the drawImage to draw an image at the given X and Y, but we need to makre sure it doesn't go beyon the canvas size
		//check that x and y is not beyond canvas size
		//sz is the width and height of the image
		while(isNotBiggerThanOrLessThanCanvas(x,y)) { //so if it is true, we want to generate another random that is in that range
		//thing is that the random.nextint() returns a number between 0 and canvas, and we do not want it to be equal to canvas
			x = randomGenerator.nextInt(CanvasX);
			y = randomGenerator.nextInt(CanvasY);
		}
		GraphicsDrawer.drawImage(i, x,y, sz,sz);
		// to draw centred at x,y, give top left position and x,y size
		// sizes/position in range 0.. canvassize 
	//gc.drawImage(i, x-sz/2, y-sz/2, sz, sz);
}
	
}
